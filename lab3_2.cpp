#include <iostream>
#include <fstream>
using namespace std;

int main()
{
	ifstream fs;
	ofstream ft;
	string str;
	string file_name_1;
	string file_name_2;

	cout << "Enter file_name_1 with Extension: ";
	cin >> file_name_1;

	fs.open(file_name_1);

	if (!fs)
	{
		cout << "Error in Opening file_name_1...!!!";
		exit(1);
	}

	cout << "Enter file_name_2 with Extension: ";

	cin>>file_name_2;

	ft.open(file_name_2);

	if (!ft)
	{
		cout << "Error in Opening file_name_2...!!!";
		fs.close();
		exit(2);
	}

	if (fs && ft)
	{
		while (getline(fs, str))
		{
			ft << str << "\n";
		}

		cout << "\n\n File_name_1 Successfully Copied to File_name_2...!!!";

	}
	else
	{
		cout << "File Cannot Open...!!!";
	}

	cout << "\n\n Open File_name_2 and Check!!!\n";

	fs.close();
	ft.close();
}
