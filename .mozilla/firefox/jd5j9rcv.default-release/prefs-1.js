// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.migrationsApplied", 12);
user_pref("app.normandy.startupRolloutPrefs.fission.experiment.enrollmentStatus", 4);
user_pref("app.normandy.startupRolloutPrefs.media.peerconnection.mtransport_process", true);
user_pref("app.normandy.startupRolloutPrefs.network.cookie.sameSite.laxByDefault", false);
user_pref("app.normandy.startupRolloutPrefs.network.cookie.sameSite.noneRequiresSecure", false);
user_pref("app.normandy.startupRolloutPrefs.network.cookie.sameSite.schemeful", false);
user_pref("app.normandy.startupRolloutPrefs.network.process.enabled", true);
user_pref("app.normandy.user_id", "30c99e22-192a-43ab-aa9d-3d81627ed63c");
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1647019890);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1647019290);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1647019650);
user_pref("app.update.lastUpdateTime.region-update-timer", 1647019170);
user_pref("app.update.lastUpdateTime.rs-experiment-loader-timer", 1647019530);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1647019410);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1647019770);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 1646927942);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1647020010);
user_pref("browser.bookmarks.addedImportButton", true);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.contentblocking.category", "standard");
user_pref("browser.contextual-services.contextId", "{0cce2a2e-b57a-4ae0-a5b3-efa4c8da4179}");
user_pref("browser.download.viewableInternally.typeWasRegistered.avif", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.svg", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.webp", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.xml", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1644654320);
user_pref("browser.laterrun.bookkeeping.sessionCount", 6);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 121);
user_pref("browser.newtabpage.activity-stream.impressionId", "{dc587493-0c4b-44a5-afc0-4e3852492466}");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"ids\":[\"bookmark\"],\"idsInUrlbar\":[\"bookmark\"],\"idsInUrlbarPreProton\":[],\"version\":1}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.proton.toolbar.version", 3);
user_pref("browser.region.update.updated", 1647019172);
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "1647028578521");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "1647030404521");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1647019168137");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1647040768137");
user_pref("browser.search.region", "PK");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1647019144");
user_pref("browser.startup.couldRestoreSession.count", 1);
user_pref("browser.startup.homepage_override.buildID", "20220106144528");
user_pref("browser.startup.homepage_override.mstone", "96.0");
user_pref("browser.startup.lastColdStartupCheck", 1647019142);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"save-to-pocket-button\",\"downloads-button\",\"fxa-toolbar-menu-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"import-button\",\"personal-bookmarks\"]},\"seen\":[\"save-to-pocket-button\",\"developer-button\"],\"dirtyAreaCache\":[\"nav-bar\",\"PersonalToolbar\",\"toolbar-menubar\",\"TabsToolbar\"],\"currentVersion\":17,\"newElementCount\":2}");
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("browser.urlbar.quicksuggest.migrationVersion", 2);
user_pref("browser.urlbar.quicksuggest.scenario", "history");
user_pref("browser.urlbar.tipShownCount.searchTip_onboard", 2);
user_pref("browser.urlbar.tipShownCount.searchTip_redirect", 4);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1644654325859");
user_pref("distribution.canonical.bookmarksProcessed", true);
user_pref("distribution.iniFile.exists.appversion", "96.0");
user_pref("distribution.iniFile.exists.value", true);
user_pref("doh-rollout.balrog-migration-done", true);
user_pref("doh-rollout.doneFirstRun", true);
user_pref("doh-rollout.home-region", "PK");
user_pref("dom.push.userAgentID", "884c46b3c78f44ab8e41b0613179aa0f");
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.databaseSchema", 34);
user_pref("extensions.getAddons.cache.lastUpdate", 1647019891);
user_pref("extensions.getAddons.databaseSchema", 6);
user_pref("extensions.lastAppBuildId", "20220106144528");
user_pref("extensions.lastAppVersion", "96.0");
user_pref("extensions.lastPlatformVersion", "96.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.pictureinpicture.enable_picture_in_picture_overrides", true);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webcompat.enable_shims", true);
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org", true);
user_pref("extensions.webextensions.uuids", "{\"doh-rollout@mozilla.org\":\"099bbf69-af27-4046-8bad-09467abf9d1a\",\"formautofill@mozilla.org\":\"c4d630cb-55c3-497b-a1da-4c509fb20ebf\",\"pictureinpicture@mozilla.org\":\"1c9bdc75-1176-4f93-83eb-538edc328a2d\",\"proxy-failover@mozilla.com\":\"dd237b3e-6283-4375-9c98-7df4b4432874\",\"screenshots@mozilla.org\":\"15cecbbc-a8e6-4f7a-944f-53a4eb5c0b5d\",\"webcompat-reporter@mozilla.org\":\"30aabe58-64f2-4581-a5ba-11646ac1923a\",\"webcompat@mozilla.org\":\"49f1a881-4b1a-4dc1-aaf1-69efaa8a9432\",\"default-theme@mozilla.org\":\"68cba4bb-ee6a-41b4-980d-1c17c6101549\",\"addons-search-detection@mozilla.com\":\"a40ffb2c-d493-46e7-8c3e-9e55a675e30d\",\"google@search.mozilla.org\":\"f428cd0b-af4f-497b-a871-d898d69b17ad\",\"amazondotcom@search.mozilla.org\":\"97fca399-15ef-437d-85de-40c54585a350\",\"wikipedia@search.mozilla.org\":\"d2e7e422-746b-48fc-a84b-f742dc4c6f29\",\"bing@search.mozilla.org\":\"1eac7adb-5f92-4785-b3e0-278397724295\",\"ddg@search.mozilla.org\":\"f2b37604-9b2e-4849-a95e-8b345ae1fd2d\"}");
user_pref("fission.experiment.max-origins.last-disqualified", 0);
user_pref("fission.experiment.max-origins.last-qualified", 1644654326);
user_pref("fission.experiment.max-origins.qualified", true);
user_pref("fission.experiment.startupEnrollmentStatus", 4);
user_pref("gfx.blacklist.layers.opengl", 4);
user_pref("gfx.blacklist.layers.opengl.failureid", "FEATURE_FAILURE_SOFTWARE_GL");
user_pref("idle.lastDailyNotification", 1647019616);
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.lastUpdate", 1644654384);
user_pref("media.gmp-gmpopenh264.version", "1.8.1.1");
user_pref("media.gmp-manager.buildID", "20220106144528");
user_pref("media.gmp-manager.lastCheck", 1647019163);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("places.database.lastMaintenance", 1646927238);
user_pref("privacy.purge_trackers.date_in_cookie_database", "0");
user_pref("privacy.purge_trackers.last_purge", "1647019616442");
user_pref("privacy.sanitize.pending", "[{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]");
user_pref("security.remote_settings.crlite_filters.checked", 1647019771);
user_pref("security.remote_settings.intermediates.checked", 1647019771);
user_pref("security.sandbox.content.tempDirSuffix", "b40801ad-15ce-4330-b523-94ba47bf1ac7");
user_pref("services.blocklist.addons-mlbf.checked", 1647019771);
user_pref("services.blocklist.gfx.checked", 1647019771);
user_pref("services.settings.clock_skew_seconds", -7598);
user_pref("services.settings.last_etag", "\"1647021399534\"");
user_pref("services.settings.last_update_seconds", 1647027629);
user_pref("services.settings.main.anti-tracking-url-decoration.last_check", 1647019771);
user_pref("services.settings.main.cfr.last_check", 1647019771);
user_pref("services.settings.main.doh-config.last_check", 1647019771);
user_pref("services.settings.main.doh-providers.last_check", 1647019771);
user_pref("services.settings.main.fxmonitor-breaches.last_check", 1647019771);
user_pref("services.settings.main.hijack-blocklists.last_check", 1647019771);
user_pref("services.settings.main.language-dictionaries.last_check", 1647019771);
user_pref("services.settings.main.message-groups.last_check", 1647019771);
user_pref("services.settings.main.nimbus-desktop-defaults.last_check", 1647019771);
user_pref("services.settings.main.nimbus-desktop-experiments.last_check", 1647019771);
user_pref("services.settings.main.normandy-recipes-capabilities.last_check", 1647019771);
user_pref("services.settings.main.partitioning-exempt-urls.last_check", 1647019771);
user_pref("services.settings.main.password-recipes.last_check", 1647019771);
user_pref("services.settings.main.password-rules.last_check", 1647019771);
user_pref("services.settings.main.personality-provider-models.last_check", 1647019771);
user_pref("services.settings.main.personality-provider-recipe.last_check", 1647019771);
user_pref("services.settings.main.pioneer-study-addons-v1.last_check", 1647019771);
user_pref("services.settings.main.public-suffix-list.last_check", 1647019771);
user_pref("services.settings.main.query-stripping.last_check", 1647019771);
user_pref("services.settings.main.search-config.last_check", 1647019771);
user_pref("services.settings.main.search-default-override-allowlist.last_check", 1647019771);
user_pref("services.settings.main.search-telemetry.last_check", 1647019771);
user_pref("services.settings.main.sites-classification.last_check", 1647019771);
user_pref("services.settings.main.top-sites.last_check", 1647027629);
user_pref("services.settings.main.url-classifier-skip-urls.last_check", 1647019771);
user_pref("services.settings.main.websites-with-shared-credential-backends.last_check", 1647019771);
user_pref("services.settings.main.whats-new-panel.last_check", 1647019771);
user_pref("services.settings.security.onecrl.checked", 1647019771);
user_pref("storage.vacuum.last.index", 1);
user_pref("storage.vacuum.last.places.sqlite", 1644654541);
user_pref("toolkit.startup.last_success", 1647019135);
user_pref("toolkit.telemetry.cachedClientID", "32411c8f-183c-4550-a068-3e777eb4c8d1");
user_pref("toolkit.telemetry.pioneer-new-studies-available", true);
user_pref("toolkit.telemetry.previousBuildID", "20220106144528");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);
