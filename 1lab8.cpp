#include<iostream>
#include <cstdlib>
using namespace std;
int SetLock=0;
void P1CriticalSection();
void P2CriticalSection();
int main()
{
P1CriticalSection();
P2CriticalSection();
}
// Start of function for critical Section of Process1
void P1CriticalSection()
{
while (SetLock!=0);
{
cout<<"Process1 is waiting\nProcess 2 is executed\n\nprocess 2 is completed now process 1 will start execution"<<endl;

exit(0);
SetLock=1;

P2CriticalSection();
cout<<"in critical Section of Process1"<<endl;

}
}
// End of function for critical Section of Process1

// Start of function for critical Section of Process2
void P2CriticalSection()
{
while(SetLock!=0);
{
cout<<"Process2 is waiting"<<endl;

exit(0);
SetLock=1;
{
cout<<"In Critical Section of Process2"<<endl;

}
}
}
