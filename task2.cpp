#include <iostream>

using namespace std;

int main()
{
    int number_of_units;
    float discount,total_cost;
    start:
    cout<<"Enter the quantity :";
    cin>>number_of_units;
    
    if(number_of_units>=1&&number_of_units<=50)
    {
        total_cost=number_of_units*199;
        cout<<"The total cost of the purchase is :"<<total_cost<<"$"<<endl;
        cout<<"No discount...!"<<endl;
    }
    else if(number_of_units<1)
    {
        cout<<"Please enter the quantity...!"<<endl;
        goto start;
    }
    else
    {
        discount=(number_of_units*199*20)/100;
        total_cost=number_of_units*199;
        cout<<"The total cost of the purchase is :"<<total_cost<<"$"<<endl;
        cout<<"The discount is :"<<discount<<"$"<<endl;
        cout<<"The total cost of the purchase after discount is :"<<total_cost-discount<<"$"<<endl;
    }
    return 0;
}
