#include <iostream>

using namespace std;
// Banker Algo

int main()
{

    // P0, P1, P2, are the Process names here
 

  int n, m, i, j, k;

  n = 3; // Number of processes

  m = 3; // Number of resources

  int alloc[3][3];
    cout<<"Enter the ALLOCATION VALUES";               
    for(int x=0;x<3;x++)
    {
    	for(int y=0;y<3;y++)
    	{
    		cin>>alloc[x][y];
		}
	}
 

  int max[5][3];
  cout<<"Enter the Request values";
      for(int x=0;x<3;x++)
    {
    	for(int y=0;y<3;y++)
    	{
    		cin>>max[x][y];
		}
	}
 

  int avail[3]={2,3,5};

  int f[n], ans[n], ind = 0;

  for (k = 0; k < n; k++) {

    f[k] = 0;

  }

  int need[n][m];

  for (i = 0; i < n; i++) {

    for (j = 0; j < m; j++)

      need[i][j] = max[i][j] - alloc[i][j];

  }

  int y = 0;

  for (k = 0; k < 3; k++) {

    for (i = 0; i < n; i++) {

      if (f[i] == 0) {
 

        int flag = 0;

        for (j = 0; j < m; j++) {

          if (need[i][j] > avail[j]){

            flag = 1;

            break;

          }

        }
 

        if (flag == 0) {

          ans[ind++] = i;

          for (y = 0; y < m; y++)

            avail[y] += alloc[i][y];

          f[i] = 1;

        }

      }

    }

  }

   

  int flag = 1;

   

  // To check if sequence is safe or not

  for(int i = 0;i<n;i++)

  {

        if(f[i]==0)

      {

        flag = 0;

        cout << "The given sequence is not safe";

        break;

      }

  }
 

  if(flag==1)

  {

    cout << "Following is the SAFE Sequence" << endl;

      for (i = 0; i < n - 1; i++)

        cout << " P" << ans[i] << " ->";

      cout << " P" << ans[n - 1] <<endl;

  }
  }
 

